import './App.css';
import ItemComponent from './components/body/ItemComponent';
import HeaderComponent from './components/header/HeaderComponent';

function App() {
  return (
    <div className="App ">
      <HeaderComponent />
      <ItemComponent />
    </div>
  );
}

export default App;
