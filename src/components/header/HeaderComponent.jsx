import React from 'react'

const HeaderComponent = () => {
    return (
        <div>
            <h1 className='bg-slate-400 p-12 text-yellow-50 font-bold text-xl'>TRY FLASS APP ONLINE</h1>
        </div>
    )
}

export default HeaderComponent