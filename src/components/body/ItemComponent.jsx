import React from 'react'
import { useState } from 'react'
import glassList from '../../data/data.json'
const ItemComponent = () => {
    const [chooseGlass, setChooseGlass] = useState(glassList[0])
    const handGlassSelection = (glass) => {
        setChooseGlass(glass);
    }
    return (
        <div className='container mx-auto px-20 font-sans p-20 '>
            <div className='grid grid-cols-2 place-items-center  '>
                <div className='left relative '>
                    <img className='rounded-lg ' src="./glassesImage/model.jpg" alt="" width={300} />
                    <div className='card ' >
                        <div className='glass absolute top-1/4 items-center justify-center flex flex-col '>
                            <img className='w-1/2 items-center rounded-md' src={chooseGlass.url} alt="" />
                        </div>
                        <div className='card__body absolute bottom-0 text-left bg-purple-600 bg-opacity-20 rounded-lg '>
                            <h2 className='text-xl text-slate-700 font-bold'>{chooseGlass.name}</h2>
                            <p className='text-xl font-bold'>{chooseGlass.price}$</p>
                            <p className=''>{chooseGlass.desc}</p>
                        </div>
                    </div>
                </div>
                <div className='right rounded-lg overflow-hidden'>
                    <img src="./glassesImage/model.jpg" alt="" width={300} />
                </div>

            </div >
            <div className="grid grid-cols-6 place-items-center pt-20" >
                {
                    glassList.map((glass) => {
                        return (
                            <div onClick={() => handGlassSelection(glass)} className="" key={glass.id}>
                                <img src={glass.url} alt="" />
                            </div>
                        )
                    })
                }
            </div >
        </div >
    )
}

export default ItemComponent
